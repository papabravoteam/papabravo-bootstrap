#!/bin/bash
sudo apt-get update &&
sudo apt-get -y install git

if [ ! -d /opt/kickstart ]; then
  git clone https://github.com/bltavares/kickstart.git /tmp/kickstart
  sudo mv /tmp/kickstart /opt
fi

if [ -z "`grep '/opt/kickstart/bin' $HOME/.bashrc`" ]; then
  echo '' >> "$HOME/.bashrc"
  echo 'export PATH=$PATH:/opt/kickstart/bin' >> "$HOME/.bashrc"
fi

if [ ! -d /opt/papabravo-kickstart ]; then
  git clone https://papabravoteam@bitbucket.org/papabravoteam/papabravo-kickstart /tmp/papabravo-kickstart
  sudo mv /tmp/papabravo-kickstart /opt
  cd /opt/papabravo-kickstart
  git config credential.helper store
fi

sudo ln -sf /opt/papabravo-kickstart/update.sh /usr/local/bin/update-workstation
export PATH=$PATH:/opt/kickstart/bin
update-workstation
